import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../shared/customer.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  customer:any;
  id:any;
  constructor(private route: ActivatedRoute,private customerService:CustomerService) { 
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCustomerById();
  }

  ngOnInit(): void {
  }
  getCustomerById() {
    this.customerService.getCustomerById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.customer = res.body;
      }
      else {
        console.log('get product by id failed!');
      }

    })
  }
}
