export interface Customer{
    id:string;
    firstName:string;
    lastName:string;
    avatar:string;
    age:number;
    address:string;
    city:string;
}