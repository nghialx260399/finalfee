import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient:HttpClient) { }

  getCustomers(): Observable<HttpResponse<Customer[]>> {
    const apiUrl = 'https://localhost:44309/api/Customers';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<Customer[]>(apiUrl, options);
  }

  getCustomerById(id:number): Observable<HttpResponse<Customer[]>> {
    const apiUrl = `https://localhost:44309/api/Customers/${id}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<Customer[]>(apiUrl, options);
  }

  updateCustomer(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44309/api/Customers';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  addCustomer(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44309/api/Customers';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.post(apiUrl,data, options);

  }

  deleteCustomer(id: string) {
    const apiUrl = `https://localhost:44309/api/Customers/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }
}
