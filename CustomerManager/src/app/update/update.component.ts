import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../shared/customer.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  id: any;
  customer: any;
  customerForm: FormGroup
  constructor(private route: ActivatedRoute, private customerService: CustomerService, private fb: FormBuilder,private router:Router) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCustomerById();
    this.customerForm = this.fb.group({
      id:"",
      firstName:['', [Validators.required, Validators.minLength(3)]],
      lastName:['', [Validators.required, Validators.minLength(3)]],
      avatar:[''],
      age:['', Validators.required],
      address:['', [Validators.required, Validators.minLength(10), Validators.maxLength(500)]],
      city:['', [Validators.required, Validators.minLength(3)]],
    });
   }

  ngOnInit(): void {
  }

  get firstName(){
    return this.customerForm.get('firstName');
  }

  get lastName(){
    return this.customerForm.get('lastName');
  }

  get avatar(){
    return this.customerForm.get('avatar');
  }

  get age(){
    return this.customerForm.get('age');
  }

  get address(){
    return this.customerForm.get('address');
  }

  get city(){
    return this.customerForm.get('city');
  }
  getCustomerById() {
    this.customerService.getCustomerById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.customer = res.body;
        this.customerForm.controls['id'].setValue(this.customer.id);
        this.customerForm.controls['firstName'].setValue(this.customer.firstName);
        this.customerForm.controls['lastName'].setValue(this.customer.lastName);
        this.customerForm.controls['avatar'].setValue(this.customer.avatar);
        this.customerForm.controls['age'].setValue(this.customer.age);
        this.customerForm.controls['address'].setValue(this.customer.address);
        this.customerForm.controls['city'].setValue(this.customer.city);
      }
      else {
        console.log('get product by id failed!');
      }

    })
  }

  Update() {
    if(!this.customerForm.invalid){
      this.customerService.updateCustomer(this.customerForm.value).subscribe(res=>{
        if(res.status==200){
          alert('update is successfully');
          this.router.navigateByUrl('/home');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }
}
