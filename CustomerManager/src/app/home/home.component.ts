import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../shared/customer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  customers:any;
  constructor(private customerService:CustomerService) { }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers(){
    this.customerService.getCustomers().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.customers=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteCustomer(id:string){
    this.customerService.deleteCustomer(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        alert('Delete successfully');
        this.getCustomers();
      }
      else{
        alert('Failed!');
      }
    })
  }

}
