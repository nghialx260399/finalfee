import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../shared/customer.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  customer: any;
  customerForm: FormGroup;
  constructor(private route: ActivatedRoute, private customerService: CustomerService, private fb: FormBuilder,private router:Router) {
    this.customerForm = this.fb.group({
      id:"",
      firstName:['', [Validators.required, Validators.minLength(3)]],
      lastName:['', [Validators.required, Validators.minLength(3)]],
      avatar:['', Validators.required],
      age:['', Validators.required],
      address:['', [Validators.required, Validators.minLength(10), Validators.maxLength(500)]],
      city:['', [Validators.required, Validators.minLength(3)]],
    });
   }

  ngOnInit(): void {
  }

  addCustomer(){
    if(!this.customerForm.invalid){
      this.customerService.addCustomer(this.customerForm.value).subscribe(res=>{
        if(res.status==200){
          alert('Add is successfully');
          this.router.navigateByUrl('/home');
        }
        else{
          alert('Failed');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get firstName(){
    return this.customerForm.get('firstName');
  }

  get lastName(){
    return this.customerForm.get('lastName');
  }

  get avatar(){
    return this.customerForm.get('avatar');
  }

  get age(){
    return this.customerForm.get('age');
  }

  get address(){
    return this.customerForm.get('address');
  }

  get city(){
    return this.customerForm.get('city');
  }
}
